package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseSerMujerPanama;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class SerMujerPanama extends TestBaseSerMujerPanama {
	
	final WebDriver driver;
	public SerMujerPanama(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	 
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "number")
	private WebElement IngresaLineaTelefonica;
	
	@FindBy(how = How.ID,using = "acepto")
	private WebElement checkMayorDeEdad;
	
	@FindBy(how = How.ID,using = "si")
	private WebElement btnContinue;
	
	

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInSerMujerPanama(String apuntaA,String ambiente) {		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Ser Mujer Panama - landing");
	
		
		//Seccion Home Publica
	
		String elemento1 = driver.findElement(By.xpath("//*[@id=\"recentHome\"]/div/div/div[1]/h2")).getText();
			Assert.assertEquals(elemento1,"BLOG");
		System.out.println(elemento1+" Visualizacion Exitosa");
			espera(500);
		
		//Seccion guias guide central - Hazlo tu misma
			
		WebElement menuSection1 = driver.findElement(By.xpath("//a[contains(text(), 'GUÍAS GUIDE CENTRAL')]"));		
			menuSection1.click();
		
			WebElement DIY = driver.findElement(By.xpath("//a[contains(text(), 'HAZLO TÚ MISMA')]"));		
				DIY.click();
				String elemento2 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
					Assert.assertEquals(elemento2,"HAZLO TÚ MISMA");
				System.out.println("Seccion HAZLO TÚ MISMA "+"Visualizacion Exitosa");
			espera(500);
				
	
	}			
	
	/*public void serMujerPanamaHogarYfamilia() {
		
		//Seccion Hogar y Familia	
		WebElement menuHogarYfamlia1 = driver.findElement(By.xpath("//a[contains(text(), 'HOGAR y FAMILIA')]"));		
		menuHogarYfamlia1.click();
			espera(500);
			
			WebElement opcion1 =driver.findElement(By.xpath("//a[@href='http://www.sermujerpa.com/category/hogar-y-familia/hogar/']"));		
			opcion1.click();
			
			
			
			
				String elemento3 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
					Assert.assertEquals(elemento3,"HOGAR");
					System.out.println("Seccion HOGAR "+"Visualizacion Exitosa");
				espera(500); 
			
			WebElement menuHogarYfamlia2 = driver.findElement(By.xpath("//a[contains(text(), 'HOGAR y FAMILIA')]"));
			menuHogarYfamlia2.click();
			
			WebElement opcion2 = driver.findElement(By.xpath("//a[@href='http://www.sermujerpa.com/category/hogar-y-familia/cocina/']"));		
			opcion2.click();
				String elemento4 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
					Assert.assertEquals(elemento4,"COCINA");
					System.out.println("Seccion COCINA "+"Visualizacion Exitosa");
				espera(500);
		
				WebElement menuHogarYfamlia3 = driver.findElement(By.xpath("//a[contains(text(), 'HOGAR y FAMILIA')]"));
				menuHogarYfamlia3.click();
				
			WebElement opcion3 = driver.findElement(By.xpath("//a[@href='http://www.sermujerpa.com/category/hogar-y-familia/decoracion/']"));		
			opcion3.click();
				String elemento5 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
					Assert.assertEquals(elemento5,"DECORACIÓN.");
					System.out.println("Seccion DECORACIÓN "+"Visualizacion Exitosa");
				espera(500);		
	}*/  
	
	public void serMujerPanamaModaYBelleza() {
		
	//Seccion MODA Y BELLEZA
		
		WebElement menuModaYbelleza1 = driver.findElement(By.xpath("//a[contains(text(), 'MODA Y BELLEZA')]"));		
		menuModaYbelleza1.click();
			espera(500);
			WebElement opcion1 = driver.findElement(By.xpath("//a[@href='http://www.sermujerpa.com/category/moda-y-belleza/moda/']"));		
			opcion1.click();
				String elemento3 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
					Assert.assertEquals(elemento3,"MODA");
					System.out.println("Seccion MODA "+"Visualizacion Exitosa");
				espera(500); 
			
			WebElement menuModaYbelleza2 = driver.findElement(By.xpath("//a[contains(text(), 'MODA Y BELLEZA')]"));
			menuModaYbelleza2.click();
			
			WebElement opcion2 = driver.findElement(By.xpath("//a[@href='http://www.sermujerpa.com/category/moda-y-belleza/bodas/']"));		
			opcion2.click();
				String elemento4 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
					Assert.assertEquals(elemento4,"BODA");
					System.out.println("Seccion BODA "+"Visualizacion Exitosa");
				espera(500);
		
				WebElement menuModaYbelleza3 = driver.findElement(By.xpath("//a[contains(text(), 'MODA Y BELLEZA')]"));
				menuModaYbelleza3.click();
				
			WebElement opcion3 =  driver.findElement(By.xpath("//a[@href='http://www.sermujerpa.com/category/moda-y-belleza/eventos/']"));		
			opcion3.click();
				String elemento5 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
					Assert.assertEquals(elemento5,"EVENTOS");
					System.out.println("Seccion EVENTOS "+"Visualizacion Exitosa");
				espera(500);	 
	}  
	
	public void serMujerPanamaMujerPresional() {
		
	//Seccion Mujer Profesional
		
		WebElement menuMujerProfesional = driver.findElement(By.xpath("//a[contains(text(), 'MUJER PROFESIONAL')]"));		
		menuMujerProfesional.click();
			String elemento1 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
				Assert.assertEquals(elemento1,"MUJER PROFESIONAL");
			System.out.println("Seccion MUJER PROFESIONAL "+"Visualizacion Exitosa");
		espera(500); 
	}
	
	public void serMujerPanamaSexoYpareja() {
		
	//Seccion Sexo y Pareja
		
		WebElement menuSexoYpareja = driver.findElement(By.xpath("//a[contains(text(), 'SEXO Y PAREJA')]"));		
		menuSexoYpareja.click();
			String elemento1 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
				Assert.assertEquals(elemento1,"SEXO Y PAREJA");
			System.out.println("Seccion SEXO Y PAREJA "+"Visualizacion Exitosa");
		espera(500);
	}
	
	public void serMujerPanamaSoyMama() {
		
	//Seccion Soy Mama
		
		WebElement menuSoyMama = driver.findElement(By.xpath("//a[contains(text(), 'SOY MAMÁ')]"));		
		menuSoyMama.click();
			String elemento1 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
				Assert.assertEquals(elemento1,"SOY MAMÁ");
			System.out.println("Seccion SOY MAMÁ "+"Visualizacion Exitosa");
		espera(500); 		
	}  
	
	public void serMujerVidaSana() {
		
	//Seccion Vida Sana
		WebElement menuVidaSana1 = driver.findElement(By.xpath("//a[contains(text(), 'VIDA SANA')]"));		
		menuVidaSana1.click();
			espera(500);
			WebElement opcion1 =driver.findElement(By.xpath("//a[@href='http://www.sermujerpa.com/category/salud-y-belleza/dieta/']"));		
			opcion1.click();
				String elemento3 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
					Assert.assertEquals(elemento3,"DIETA");
					System.out.println("Seccion DIETA "+"Visualizacion Exitosa");
				espera(500); 
				
			WebElement menuVidaSana2 = driver.findElement(By.xpath("//a[contains(text(), 'VIDA SANA')]"));
			menuVidaSana2.click();
					
			WebElement opcion2 = driver.findElement(By.xpath("//a[@href='http://www.sermujerpa.com/category/salud-y-belleza/ejercicios/']"));		
			opcion2.click();
				String elemento4 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
					Assert.assertEquals(elemento4,"EJERCICIOS");
					System.out.println("Seccion EJERCICIOS "+"Visualizacion Exitosa");
				espera(500);
				
				WebElement menuVidaSana3 = driver.findElement(By.xpath("//a[contains(text(), 'VIDA SANA')]"));
				menuVidaSana3.click();
						
			WebElement opcion3 = driver.findElement(By.xpath("//a[@href='http://www.sermujerpa.com/category/salud-y-belleza/salud/']"));		
			opcion3.click();
				String elemento5 = driver.findElement(By.xpath("//*[@id=\"archive-wrapper\"]/div/div/div[1]/div/h2")).getText();
					Assert.assertEquals(elemento5,"SALUD");
					System.out.println("Seccion SALUD "+"Visualizacion Exitosa");
				espera(500);
		
	}
	
	/*public void SerMujerSearchBox() {
		
	//Verifica si el searchbox funciona correctamente.
		
	 driver.findElement(By.cssSelector(".fas.fa-search")).click();
		espera(500);	
		driver.findElement(By.id("s")).sendKeys("musica"+ Keys.RETURN);
		
		String elemento3 = driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[1]/div/h2")).getText();
		Assert.assertEquals(elemento3,"RESULTADOS PARA: MUSICA");
	
		System.out.println("El buscador funciona correctamente");
		
		
		System.out.println("Resultado Esperado:Deberan visualizarce las secciones de ser Mujer Panama"); 
		System.out.println();  
		System.out.println("Fin de Test Patitas Ser Mujer Panama - Landing");
		
	}*/
}